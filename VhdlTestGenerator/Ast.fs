﻿namespace Ast
open System

type DesignFile = DesignFile of DesignUnit list

and DesignUnit = DesignUnit of ContextClause option * LibraryUnit

and ContextClause = ContextClause of ContextItem list

and ContextItem =
    | LibraryClause of Identifier list
    | UseClause of SelectedName list

and LibraryUnit =
    | PrimaryUnit of PrimaryUnit
    | SecondaryUnit of SecondaryUnit

and PrimaryUnit =
    | EntityDeclaration of EntityDeclaration
    | PackageDeclaration of Identifier * PackageDeclarativePart * Identifier option
    override x.ToString() =
        match x with
        | EntityDeclaration (Identifier name, EntityHeader (gen, port), endname) ->
            let generic = 
                match gen with
                | Some (GenericClause (InterfaceList lst)) -> 
                    sprintf "\n\tgeneric (%s);\n" (String.concat "; " (List.map (fun x -> x.ToString()) lst))
                | None -> ""
            let ports =
                match port with
                | Some (PortClause (InterfaceList lst)) ->
                    sprintf "\n\tport (%s);\n" (String.concat "; " (List.map (fun x -> x.ToString()) lst))
                | None -> ""
            let ename =
                match endname with
                | Some (Identifier n) -> n
                | None -> ""
            sprintf "entity %s is %s%send %s;\n" name generic ports ename
        | PackageDeclaration (Identifier name, PackageDeclarativePart part, endname) -> "package"

and SecondaryUnit =
    | ArchitectureBody
    | PackageBody

and EntityDeclaration = Identifier * EntityHeader * Identifier option

and EntityHeader = 
    | EntityHeader of GenericClause option * PortClause option
    override x.ToString() =
        match x with
        | EntityHeader (gen, port) ->
            let g = match gen with
                    | Some g -> g.ToString()
                    | None -> ""
            let p = match port with
                    | Some p -> p.ToString()
                    | None -> ""
            sprintf "%s\n%s" g p

and GenericClause = 
    | GenericClause of InterfaceList
    override x.ToString() =
        match x with
        | GenericClause lst -> sprintf "generic (%s);" (lst.ToString())

and PortClause = 
    | PortClause of InterfaceList
    override x.ToString() =
        match x with
        | PortClause lst -> sprintf "port (%s);" (lst.ToString())

and PackageDeclarativePart = PackageDeclarativePart of PackageDeclarativeItem list

and PackageDeclarativeItem = PackageDeclarativeItem

and InterfaceList =
    | InterfaceList of InterfaceDeclaration list
    override x.ToString() =
        match x with
        | InterfaceList lst -> String.concat "; " (List.map (fun t -> t.ToString()) lst)

and InterfaceDeclaration =
    | InterfaceVariableDeclaration of Identifier list * Mode option * SubtypeIndication * Expr option
    | InterfaceFileDeclaration of Identifier list * SubtypeIndication
    | InterfaceSignalDeclaration of Identifier list * Mode option * SubtypeIndication * Expr option
    | InterfaceConstantDeclaration of Identifier list * SubtypeIndication * Expr option
    override x.ToString() =
        let identlist lst = String.concat ", " (List.map (fun x -> x.ToString()) lst)
        let optionToString m =
            match m with
                | Some m -> m.ToString()
                | None -> ""
        let exprToString e =
            match e with
                | Some exp -> " := " + exp.ToString()
                | None -> ""
        match x with
        | InterfaceVariableDeclaration (lst, modeopt, subtype, expropt) ->
            let idents = identlist lst
            let mode = optionToString modeopt
            let expr = exprToString expropt
            sprintf "variable %s: %s %s%s" idents mode (subtype.ToString()) expr
        | InterfaceFileDeclaration (lst, subtype) ->
            let idents = identlist lst
            let stype = subtype.ToString()
            sprintf "file %s: %s" idents stype
        | InterfaceSignalDeclaration (lst, modeopt, subtype, expropt) ->
            let ids = identlist lst
            let mode = optionToString modeopt
            let stype = subtype.ToString()
            let expr = exprToString expropt
            sprintf "signal %s: %s %s" ids mode stype
        | InterfaceConstantDeclaration (lst, subtype, expropt) -> 
            let ids = identlist lst
            let stype = subtype.ToString()
            let expr = exprToString expropt
            sprintf "constant %s: %s %s" ids stype expr
 
and Mode = 
    | IN | OUT | INOUT | BUFFER | LINKAGE
    override x.ToString() = 
        match x with
        | IN -> "IN" | OUT -> "OUT" | INOUT -> "INOUT" | BUFFER -> "BUFFER" | LINKAGE -> "LINKAGE"

and SubtypeIndication = 
    | SubtypeIndication of Identifier * Identifier option * Constraint option
    override x.ToString() =
        match x with
        | SubtypeIndication (Identifier pref, s, c) ->
            let suff =  match s with
                        | Some (Identifier i) -> i
                        | None -> ""
            let constr = match c with
                         | Some cons -> cons.ToString()
                         | None -> ""
            sprintf "%s %s %s" pref suff constr

and Constraint =
    | RangeConstraint of RangeConstraintT
    | IndexConstraint of IndexConstraintT
    override x.ToString() =
        match x with
        | RangeConstraint r -> r.ToString()
        | IndexConstraint i -> i.ToString()

and RangeConstraintT = 
    | RangeAttributeName of AttributeName
    | RangeExpr of Expr * Direction * Expr
    override x.ToString() =
        match x with
        | RangeAttributeName a -> a.ToString()
        | RangeExpr (e1, d, e2) -> sprintf "%s %s %s" (e1.ToString()) (d.ToString()) (e2.ToString())

and Direction =
    | TO
    | DOWNTO
    override x.ToString() = match x with TO -> "to" | DOWNTO -> "downto" 

and IndexConstraintT = 
    | IndexConstraintT of DiscreteRange list
    override x.ToString() =
        match x with
        | IndexConstraintT lst -> String.concat ", " (List.map (fun x -> x.ToString()) lst)

and DiscreteRange =
    | RangeWithSubtype of SubtypeIndication
    | RangeWithConstraint of RangeConstraintT
    override x.ToString() =
        match x with
        | RangeWithSubtype s -> s.ToString()
        | RangeWithConstraint r -> r.ToString()

and Expr = 
    | Literal of Literal
    | StringLiteral of StringLiteral
    | CharLiteral of CharLiteral
    override x.ToString() =
        match x with
        | Literal l -> l.ToString()
        | StringLiteral s -> s.ToString()
        | CharLiteral c -> c.ToString()

and Identifier = 
    | Identifier of string
    override x.ToString() = match x with Identifier s -> s

and SelectedName = 
    | SelectedName of string
    override x.ToString() = match x with SelectedName s -> s

and AttributeName = 
    | AttributeName of Identifier * Identifier * Expr option
    override x.ToString() = match x with AttributeName (p, s, _) -> sprintf "%O'%O" p s

and Literal =
    | Literal of string
    override x.ToString() = match x with Literal s -> s

and StringLiteral =
    | StringLiteral of string
    override x.ToString() = match x with StringLiteral s -> s

and CharLiteral =
    | CharLiteral of string
    override x.ToString() = match x with CharLiteral s -> s
