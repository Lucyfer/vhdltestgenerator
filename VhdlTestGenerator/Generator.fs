﻿module Generator

open System
open Ast

let private testbench_comment = "-- VHDL UNIT TEST FILE --\n"

let private testbench_entity_title = 
    String.concat "\n" [
        "-- A testbench has no ports." 
        "entity entity_tb is end;"; ""]

let private testbench_arch_title =
    String.concat "\n" [
        "architecture behav of entity_tb is"; ""]

let private testbench_component_title =
    String.concat "\n" [
        "-- Declaration of the component that will be instantiated."
        "component test_entity"; ""]

let private testbench_component_end =
    "end component;\n"

let private generate_generic (ast: EntityDeclaration) =
    let (_, EntityHeader (clause, _), _) = ast
    match clause with
    | Some gen -> gen.ToString() + "\n"
    | None -> ""  

let private generate_port (ast: EntityDeclaration) =
    let (_, EntityHeader (_, clause), _) = ast
    match clause with
    | Some port -> port.ToString() + "\n"
    | None -> ""

let private namelist (ast: EntityDeclaration) : (string list * SubtypeIndication * Mode option) seq =
    let (_, EntityHeader (_, clause), _) = ast
    match clause with
    | Some (PortClause (InterfaceList lst)) -> seq {
        for port in lst do
            match port with
            | InterfaceSignalDeclaration (lst, modeop, subtype, _) ->          
                let slst = List.map (fun (Identifier n) -> n) lst
                yield (slst, subtype, modeop)
            | _ -> ()
        }
    | None -> Seq.empty

let private generate_signals (ast: EntityDeclaration) =
    let builder = new Text.StringBuilder()
    for (nlst, stype, _) in namelist ast do
        builder.Append(sprintf "signal %s: %s;\n"  (String.concat ", " nlst) (stype.ToString())) |> ignore
    builder.ToString()

let private testbench_use_entity (ast: EntityDeclaration) =
    let (Identifier name, _, _) = ast
    "for entity_0: " + name + " use entity work." + name + ";\n"

let private testbench_portmap (ast: EntityDeclaration) =
    let (Identifier name, _, _) = ast
    let nlist = namelist ast
    let mapping = String.concat ", " 
                    (Seq.map (fun x -> sprintf "%s=>%s" x x) 
                    (seq { for (n, _, _) in nlist do yield! n}))
    sprintf "entity_0: %s port map (%s);\n" name mapping

let private testbench_begin = "begin\n"

let private testbench_end = "end;"

let private testbench_pattern_type (ast: EntityDeclaration) =
    let items = String.concat "\n" 
                    (Seq.map (fun (lst, subtype, _) ->
                        let slist = String.concat ", " lst
                        let stype = subtype.ToString()
                        sprintf "  %s: %s;" slist stype) 
                    (namelist ast))
    String.concat "\n" [
        "type pattern_type is record"
        "-- The inputs of the adder."
        items
        "end record;"]

let testbench_pattern_array (ast: EntityDeclaration) =
    let items = Seq.map (fun t -> sprintf "'<%s>'" t) (seq { for (x, _, _) in namelist ast do yield! x })
    let str = sprintf "-- Fill with set of the patterns (%s)" (String.concat ", " items)
    String.concat "\n" [
        "type pattern_array is array (natural range <>) of pattern_type;"
        "constant patterns : pattern_array :="
        "("; str; ");"]

let testbench_run_tests (ast: EntityDeclaration) =
    let sq = namelist ast
    let inputs = String.concat "\n" (seq { for (lst, _, modeop) in sq do
                                                let create_pattern (item: string) = 
                                                    String.Format("{0} <= patterns(i).{0};", item)
                                                match modeop with 
                                                | None | Some IN -> yield String.concat "\n" (List.map create_pattern lst)
                                                | _ -> () })
    let outputs = String.concat "\n" (seq { for (lst, _, modeop) in sq do
                                                let create_assert (item: string) = 
                                                    String.Format("assert {0} = patterns(i).{0}\n  report \"bad value on '{0}'\" severity error;", item)
                                                match modeop with
                                                | Some OUT -> yield String.concat "\n" (List.map create_assert lst)
                                                | _ -> () })
    String.concat "\n" [
        "-- Check each pattern."        
        "for i in patterns'range loop"
        "-- Set the inputs."
        inputs
        "-- Wait for the results."
        "wait for 1 ns;"
        "-- Check the outputs."
        outputs
        "end loop;"]

let private testbench_process (ast: EntityDeclaration) =
    String.concat "\n" [
        "process";
            testbench_pattern_type ast;
            testbench_pattern_array ast;
        "begin";
            testbench_run_tests ast;
        "assert false report \"end of test\" severity note;";
        "-- Wait forever; this will finish the simulation.";
        "wait;";
        "end process;\n"]

let generate (ast: EntityDeclaration) =
    String.concat "" [
        testbench_comment;
        testbench_entity_title;
        testbench_arch_title;
            testbench_component_title;
                generate_generic ast;
                generate_port ast;
            testbench_component_end;
            testbench_use_entity ast;
            generate_signals ast;
        testbench_begin;
            testbench_portmap ast;
            testbench_process ast;
        testbench_end]
