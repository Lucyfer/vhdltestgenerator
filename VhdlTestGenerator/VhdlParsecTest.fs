﻿namespace VhdlParsecTests

open FParsec
open Ast
open VhdlParsec
open Microsoft.VisualStudio.TestTools.UnitTesting

[<TestClass>] 
type VhdlParserTestCases ()=

    [<TestMethod>]
    member x.testComments ()=
        let input = " --line1 \n --line2 \n -- "
        match run comments input with
        | Success (res,state,pos) -> Assert.AreEqual(String.length input, pos.Index |> int)
        | Failure (str, err, _) -> Assert.Fail(str)

    [<TestMethod>]
    member x.testMatchString ()=
        match run (match_str "id") "id \t \n" with
        | Success (res, _, pos) -> Assert.AreEqual("id", res)
        | Failure (str, err, _) -> Assert.Fail(str)

        match run (match_str ".") ". --! comment \r\n  \t" with
        | Success (res, _, _) -> Assert.AreEqual(".", res)
        | Failure (str, err, _) -> Assert.Fail(str)

    [<TestMethod>]
    member x.testIdentifier ()=
        match run identifierp "" with
        | Success (res,_,_) -> Assert.Fail(res.ToString())
        | Failure (str,_,_) -> Assert.IsNotNull(str)

        match run identifierp " \r  \n  \t " with
        | Success (res,_,_) -> Assert.Fail()
        | Failure (str,_,_) -> Assert.IsNotNull(str)

        let input = "a \n "
        match run identifierp input with
        | Success (Identifier res,_,pos) -> 
            Assert.AreEqual("a", res)
            Assert.AreEqual(String.length input, int <| pos.Index)
        | Failure (str,_,_) -> Assert.Fail(str)

        match run identifierp "_" with
        | Success (res,_,_) -> Assert.Fail(res.ToString())
        | Failure (str,_,_) -> Assert.AreNotEqual("", str)

        match run identifierp "a_b_c_1_234567_DEFG" with
        | Success (Identifier res,_,_) -> Assert.AreEqual("a_b_c_1_234567_DEFG", res)
        | Failure (str,_,_) -> Assert.Fail(str)

        match run identifierp "bus id" with
        | Success (res,_,_) -> Assert.Fail(res.ToString())
        | Failure (err,_,_) -> ()

        match run (optp identifierp) "id other_id" with
        | Success (Some (Identifier "id"),_,_) -> ()
        | Success (res,_,_) -> Assert.Fail(res.ToString())
        | Failure (err,_,_) -> Assert.Fail(err)

        match run (opt identifierp) "INOUT other_id" with
        | Success (Some i,_,_) -> Assert.Fail(string i)
        | Success (None,_,_) -> ()
        | Failure (err,_,_) -> Assert.Fail(err)

    [<TestMethod>]
    member x.testAttributeName ()=
        match run attribute_name "" with
        | Success (res,_,_) -> Assert.Fail(res.ToString())
        | Failure (str,_,_) -> Assert.AreNotEqual("", str)

        match run attribute_name "simple_name ' range" with
        | Success (AttributeName (Identifier name, Identifier suffix, None),_,_) ->
            Assert.AreEqual("simple_name", name)
            Assert.AreEqual("range", suffix)
        | Failure (str,_,_) -> Assert.Fail(str)
        | _ -> Assert.Fail()

        match run attribute_name "complex . name ' length ( 6 )" with
        | Success (AttributeName (Identifier name, Identifier suffix, Some (Expr.Literal (Literal "6"))),_,_) ->
            Assert.AreEqual("complex.name", name)
            Assert.AreEqual("length", suffix)
        | Failure (str,_,_) -> Assert.Fail(str)
        | _ -> Assert.Fail()

    [<TestMethod>]
    member x.testPrefix ()=
        match run prefix "" with
        | Success (res,_,_) -> Assert.Fail(res.ToString())
        | Failure (str,_,_) -> Assert.AreNotEqual("", str)

        match run prefix "\r  \n  \t" with
        | Success (res,_,_) -> Assert.Fail(res.ToString())
        | Failure (str,_,_) -> Assert.AreNotEqual("", str)

        match run prefix "prefix\r  \n \t" with
        | Success (Identifier res,_,_) -> Assert.AreEqual("prefix", res) 
        | Failure (str,_,_) -> Assert.Fail(str)

        match run prefix "prefix . suffix1 . suf_2" with
        | Success (Identifier res,_,_) -> Assert.AreEqual("prefix.suffix1.suf_2", res) 
        | Failure (str,_,_) -> Assert.Fail(str)

    [<TestMethod>]
    member x.testEntity ()=
        match run entity_declaration "-- empty" with
        | Success (res,_,_) -> Assert.Fail(res.ToString())
        | Failure (err,_,_) -> ()

        match run entity_declaration "Entity entity_1 IS end ; " with
        | Success _ -> ()
        | Failure (str,_,_) -> Assert.Fail(str)

        match run entity_declaration "entity e is generic(constant a,b,c:std_logic; d:bit range 7 downto 1); port(e: inout bit bus:=123);begin end;" with
        | Success _ -> ()
        | Failure (str,_,_) -> Assert.Fail(str)

        match run entity_declaration "entity e is generic ( a, b, c : std_logic ) ; port (signal d,e: bit := \"001\" ); end ; " with
        | Success _ -> ()
        | Failure (str,_,_) -> Assert.Fail(str)

    [<TestMethod>]
    member x.testIdentifierList ()=
        match run identifier_list "a , --!@#$\n b , c" with
        | Success (lst,_,_) -> 
            Assert.AreEqual(3, lst.Length)
            let (Identifier a, Identifier b, Identifier c) = lst.[0], lst.[1], lst.[2]
            Assert.AreEqual("a", a)
            Assert.AreEqual("b", b)
            Assert.AreEqual("c", c)
        | Failure (str,_,_) -> Assert.Fail(str)

    [<TestMethod>]
    member x.testConstantDecl ()=
        match run interface_constant_declaration "constant a, b, c : In Std_logic := 3 ;" with
        | Success (res,_,_) ->
            match res with
            | InterfaceConstantDeclaration (lst, t, Some (Expr.Literal (Literal e))) -> 
                Assert.AreEqual(3, lst.Length)
                Assert.AreEqual("3", e)
            | _ -> Assert.Fail(res.ToString())
        | Failure (str,_,_) -> Assert.Fail(str)

    [<TestMethod>]
    member x.testSignalDecl ()=
        match run interface_signal_declaration "a,b,c : inout std_logic bus :=4" with
        | Success (res,_,_) ->
            match res with
            | InterfaceSignalDeclaration (lst, Some INOUT, SubtypeIndication (Identifier "std_logic", None, None), Some e) ->
                Assert.AreEqual(3, lst.Length)
            | _ -> Assert.Fail(res.ToString())
        | Failure (err,_,_) -> Assert.Fail(err)

    [<TestMethod>]
    member x.testGenericClause ()=
        match run generic_clause "generic ( constant a, b : in sometype := 1; signal c: bit bus ; file d: filetype ) ;" with
        | Success (res,_,_) -> ()
        | Failure (str,_,_) -> Assert.Fail(str)

        match run generic_clause "generic ( signal a, b : out sometype ) ;" with
        | Success (res,_,_) -> ()
        | Failure (str,_,_) -> Assert.Fail(str)

        match run generic_clause "generic ( file a, b : sometype ) ;" with
        | Success (res,_,_) -> ()
        | Failure (str,_,_) -> Assert.Fail(str)

        match run generic_clause "generic ( variable a, b : Buffer sometype ) ;" with
        | Success (res,_,_) -> ()
        | Failure (str,_,_) -> Assert.Fail(str)

        match run generic_clause "generic ( a, b : InOut sometype BUS := 000 ) ;" with
        | Success (res,_,_) -> ()
        | Failure (str,_,_) -> Assert.Fail(str)

    [<TestMethod>]
    member x.testPortClause ()=
        match run port_clause "port ( );" with
        | Success (res,_,_) -> Assert.Fail("expected decl list")
        | Failure (err,_,_) -> ()

        match run port_clause "port(a:bit)" with
        | Success (res,_,_) -> Assert.Fail("expected semicolon")
        | Failure (err,_,_) -> ()

        match run port_clause "port(a:bit);" with
        | Success (res,_,_) -> ()
        | Failure (err,_,_) -> Assert.Fail(err)

        match run port_clause "port ( a,b,c : inout std_logic bus:=4 ; signal d,e,f,g : std_logic2 );" with
        | Success (PortClause (InterfaceList res),_,_) ->
            Assert.AreEqual(2, res.Length)
            let lst1, lst2 = res.[0], res.[1]
            match lst1 with
            | InterfaceSignalDeclaration (lst, Some INOUT, SubtypeIndication (Identifier "std_logic", None, None), e) ->
                Assert.AreEqual(lst.Length, 3)
            | _ -> Assert.Fail()
            match lst2 with
            | InterfaceSignalDeclaration (lst, m, SubtypeIndication (Identifier "std_logic2", None, None), None) ->
                Assert.AreEqual(lst.Length, 4)
                match m with
                | None -> ()
                | _ -> Assert.Fail(string m)
            | _ -> Assert.Fail()
        | Failure (err,_,_) -> Assert.Fail(err)

    [<TestMethod>]
    member x.testMode ()=
        match run mode "IN" with
        | Success (IN,_,_) -> ()
        | Success (m,_,_) -> Assert.Fail(m.ToString())
        | Failure (err,_,_) -> Assert.Fail(err)

        match run mode "INOUT" with
        | Success (INOUT,_,_) -> ()
        | Success (m,_,_) -> Assert.Fail(m.ToString())
        | Failure (err,_,_) -> Assert.Fail(err)

        match run mode "BUFFER" with
        | Success (BUFFER,_,_) -> ()
        | Success (m,_,_) -> Assert.Fail(m.ToString())
        | Failure (err,_,_) -> Assert.Fail(err)

    [<TestMethod>]
    member x.testSubtypeIndication ()=
        match run subtype_indication "simple_type" with
        | Success (SubtypeIndication (Identifier res, None, None), _, _) -> Assert.AreEqual("simple_type", res)
        | Failure (err,_,_) -> Assert.Fail(err)
        | _ -> Assert.Fail()

        match run subtype_indication "simple_type signal" with
        | Success (SubtypeIndication (Identifier res, None, None), _, _) -> Assert.AreEqual("simple_type", res)
        | Failure (err,_,_) -> Assert.Fail(err)
        | _ -> Assert.Fail()

        match run subtype_indication "resolution thetype" with
        | Success (SubtypeIndication (Identifier res, Some (Identifier t), None), _, _) -> 
            Assert.AreEqual("resolution", res)
            Assert.AreEqual("thetype", t)
        | Failure (err,_,_) -> Assert.Fail(err)
        | _ -> Assert.Fail()

        match run subtype_indication "type1 type2 Range 1 to 3" with
        | Success (SubtypeIndication (Identifier res, Some (Identifier t2), Some (RangeConstraint (RangeExpr (f, TO, t)))), _, _) -> 
            Assert.AreEqual("type1", res)
            Assert.AreEqual("type2", t2)
            match f with
            | Expr.Literal (Literal v1) -> Assert.AreEqual("1", v1)
            | _ -> Assert.Fail()
            match t with
            | Expr.Literal (Literal v2) -> Assert.AreEqual("3", v2)
            | _ -> Assert.Fail()
        | Failure (err,_,_) -> Assert.Fail(err)
        | _ -> Assert.Fail()

        match run ((skip_str ":" >>. optp mode) >>. subtype_indication .>> optional (match_str "BUS")) ": in std_logic stype bus" with
        | Success (SubtypeIndication (Identifier "std_logic", Some (Identifier "stype"), None),_,_) -> ()
        | Failure (err,_,_) -> Assert.Fail(err)
        | _ -> Assert.Fail()

        match run ((skip_str ":" >>. optp mode) >>. subtype_indication .>> optional (match_str "BUS")) ": in std_logic ( 0 to 4 , attr_range ) bus" with
        | Success (SubtypeIndication (Identifier "std_logic", None, Some (IndexConstraint constrt)),_,_) -> ()
        | Failure (err,_,_) -> Assert.Fail(err)
        | _ -> Assert.Fail()

    [<TestMethodAttribute>]
    member x.testIndexConstraint ()=
        match run index_constraint "(  )" with
        | Success (_,_,_) -> Assert.Fail()
        | Failure (err,_,_) -> ()

        match run index_constraint "( attrib )" with
        | Success (IndexConstraintT lst,_,_) ->
            Assert.AreEqual(1, lst.Length)
            match lst.Head with
            | RangeWithSubtype (SubtypeIndication (Identifier "attrib", None, None)) -> ()
            | _ -> Assert.Fail()
        | Failure (err,_,_) -> Assert.Fail(err)

        match run index_constraint "( attrib , 128 downto 0 , sub type_ ( nested_attr ) )" with
        | Success (IndexConstraintT lst,_,_) ->
            Assert.AreEqual(3, lst.Length)
            match lst.[1] with
            | RangeWithConstraint (RangeExpr (Expr.Literal (Literal "128"), DOWNTO, Expr.Literal (Literal "0"))) -> ()
            | _ -> Assert.Fail()
        | Failure (err,_,_) -> Assert.Fail(err)

    [<TestMethod>]
    member x.testCharLiteral ()=
        match run char_literal "" with
        | Success (res,_,_) -> Assert.Fail(res.ToString())
        | Failure (err,_,_) -> ()

        match run char_literal "'" with
        | Success (res,_,_) -> Assert.Fail(res.ToString())
        | Failure (err,_,_) -> ()

        match run char_literal "''" with
        | Success (res,_,_) -> Assert.Fail(res.ToString())
        | Failure (err,_,_) -> ()

        match run char_literal "'ab'" with
        | Success (res,_,_) -> Assert.Fail(res.ToString())
        | Failure (err,_,_) -> ()

        match run char_literal "'!'" with
        | Success (CharLiteral res,_,_) -> Assert.AreEqual("!", res)
        | Failure (err,_,_) -> Assert.Fail(err.ToString())

        match run char_literal @"'\''" with
        | Success (CharLiteral res,_,_) -> Assert.AreEqual(@"\'", res)
        | Failure (err,_,_) -> Assert.Fail(err.ToString())

        match run char_literal @"'\n'" with
        | Success (CharLiteral res,_,_) -> Assert.AreEqual(@"\n", res)
        | Failure (err,_,_) -> Assert.Fail(err.ToString())

    [<TestMethod>]
    member x.testStringLiteral ()=
        match run string_literal "" with
        | Success (res,_,_) -> Assert.Fail(res.ToString())
        | Failure (err,_,_) -> ()

        match run string_literal "\"" with
        | Success (res,_,_) -> Assert.Fail(res.ToString())
        | Failure (err,_,_) -> ()

        match run string_literal "\"\"" with
        | Success (StringLiteral res,_,_) -> Assert.AreEqual("", res)
        | Failure (err,_,_) -> ()

        match run string_literal "\"#\"" with
        | Success (StringLiteral res,_,_) -> Assert.AreEqual("#", res)
        | Failure (err,_,_) -> Assert.Fail(err.ToString())

        match run string_literal "\" string \"" with
        | Success (StringLiteral res,_,_) -> Assert.AreEqual(" string ", res)
        | Failure (err,_,_) -> Assert.Fail(err.ToString())

        match run string_literal "\" \\\" ' abc \"" with
        | Success (StringLiteral res,_,_) -> Assert.AreEqual(" \" ' abc ", res)
        | Failure (err,_,_) -> Assert.Fail(err.ToString())

    [<TestMethod>]
    member x.testDesignFile ()=
        match run design_file "" with
        | Success (res,_,_) -> Assert.Fail(res.ToString())
        | Failure (err,_,_) -> ()

        match run design_file "-- empty" with
        | Success (res,_,_) -> Assert.Fail(res.ToString())
        | Failure (err,_,_) -> ()

        match run design_file "library ieee; use ieee.std_logic_1164.all;" with
        | Success (res,_,_) -> Assert.Fail(res.ToString())
        | Failure (err,_,_) -> ()

        match run design_file "library ieee; use ieee.std_logic_1164.all; entity e is end;" with
        | Success (DesignFile (DesignUnit (Some (ContextClause context), PrimaryUnit (EntityDeclaration entity)) ::_),_,_) ->
            let (Identifier name, _, _) = entity
            Assert.AreEqual("e", name)
        | Failure (err,_,_) -> Assert.Fail(err.ToString())
        | _ -> Assert.Fail("incorrect parser")
