﻿open System
open System.IO
open System.Collections.Generic
open FParsec
open Ast
open VhdlParsecTests
open VhdlParsec
open Generator

let private generate_unit_test utf8content : List<string> =
    let result = new List<string>()
    match run design_file utf8content with
    | Success (DesignFile units,_,_) ->
        for u in units do
            match u with
            | DesignUnit (_, PrimaryUnit (EntityDeclaration ent)) -> result.Add(generate ent)
            | _ -> ()
        result
    | Failure (err,_,_) ->
        failwith err

[<EntryPoint>]
let main args =
    if args.Length = 2 then
        let input, output = args.[0], args.[1]
        try
            let content = File.ReadAllText(input, System.Text.Encoding.UTF8)
            let res = generate_unit_test content
            File.WriteAllLines(output, res)
            0
        with
        | e -> 
            printfn "Error: %s" e.Message
            2
    else if args.Length = 1 then
        try
            let input = args.[0]
            let content = File.ReadAllText(input, System.Text.Encoding.UTF8)
            let res = generate_unit_test content
            for line in res do
                printfn "%s" line
            0
        with
        | e ->
            printfn "Error: %s" e.Message
            2
    else 
        let content = Console.In.ReadToEnd()
        let res = generate_unit_test content
        for line in res do
            printfn "%s" line
        0 
