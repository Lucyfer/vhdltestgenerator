﻿module VhdlParsec

open FParsec
open Ast
open System

let comments s = skipMany ((skipString "--" .>> skipRestOfLine true) <|> spaces1) <| s
let skip_str str = comments >>. skipStringCI str .>> comments
let match_str str = pstringCI str .>> comments
let optionalp parser s = optional parser .>> comments <| s
let optp parser s = opt parser .>> comments <| s
let many1p parser s = many1 parser .>> comments <| s
let constant v = fun _ -> v

let private keyWordSet =
    System.Collections.Generic.HashSet<_> (
        [|"BUS"; "IN"; "INOUT"; "OUT"; "BUFFER"; "LINKAGE"; "SIGNAL"; "CONSTANT"; "VARIABLE"; "FILE"; "RANGE"; "TYPE"; "SUBTYPE"|]
    )

let rec design_file s = comments >>. many1p design_unit |>> DesignFile <| s

and design_unit s = optp context_clause .>>. library_unit |>> DesignUnit <| s

and context_clause s = many1p context_item |>> ContextClause <| s

and context_item s = library_clause <|> use_clause <| s

and library_clause s = match_str "LIBRARY" >>. identifier_list .>> skip_str ";" |>> LibraryClause <| s

and use_clause s = match_str "USE" >>. selected_name_list .>> skip_str ";" |>> UseClause <| s

and library_unit s = primary_unit <|> secondary_unit <| s

and primary_unit s = (entity_declaration <|> package_declaration) |>> PrimaryUnit <| s

and entity_declaration s = 
    (tuple3 
        (match_str "ENTITY" >>. identifierp .>> skip_str "IS")
        (entity_header .>> (optionalp (match_str "BEGIN") >>. skip_str "END" >>. optionalp (match_str "ENTITY")))
        (optp identifierp .>> match_str ";")) |>> EntityDeclaration <| s

and entity_header s = (optp generic_clause) .>>. (optp port_clause) |>> EntityHeader <| s

and generic_clause s = (match_str "GENERIC" >>. between (match_str "(") (match_str ")") interface_list) .>> match_str ";" |>> GenericClause <| s

and port_clause s = (match_str "PORT" >>. between (match_str "(") (match_str ")") entity_interface_list) .>> match_str ";" |>> PortClause <| s

and package_declaration s = pzero |>> PackageDeclaration <| s

and secondary_unit s = architecture_body <|> package_body |>> SecondaryUnit <|s

and architecture_body s = pzero s

and package_body s = pzero s

and entity_interface_list s = sepBy1 interface_signal_declaration (match_str ";") |>> InterfaceList <| s

and interface_list s = 
    comments >>. sepBy1 (choice [interface_variable_declaration; 
                                 interface_file_declaration;
                                 interface_constant_declaration;
                                 interface_signal_declaration]) 
                        (match_str ";") |>> InterfaceList <| s

and interface_variable_declaration s = 
    (tuple4 
        (match_str "VARIABLE" >>. identifier_list)
        (skip_str ":" >>. optp mode)
        (subtype_indication)
        (optp (match_str ":=" >>. expr))) .>> comments |>> InterfaceVariableDeclaration <| s

and interface_file_declaration s = 
    tuple2 
        (match_str "FILE" >>. identifier_list) 
        (skip_str ":" >>. subtype_indication) .>> comments |>> InterfaceFileDeclaration <| s

and interface_signal_declaration s = 
    tuple4 
        (optionalp (match_str "SIGNAL") >>. identifier_list)
        (skip_str ":" >>. optp mode)
        (subtype_indication .>> optional (match_str "BUS"))
        (optp (match_str ":=" >>. expr)) .>> comments |>> InterfaceSignalDeclaration <| s

and interface_constant_declaration s =
    tuple3
        (match_str "CONSTANT" >>. identifier_list .>> match_str ":")
        (optionalp (match_str "IN") >>. subtype_indication)
        (optp (match_str ":=" >>. expr)) .>> comments |>> InterfaceConstantDeclaration <| s

and mode s = choice [match_str "LINKAGE" >>% LINKAGE;
                     match_str "BUFFER" >>% BUFFER;
                     match_str "INOUT" >>% INOUT;
                     match_str "OUT" >>% OUT; 
                     match_str "IN" >>% IN] <| s

and identifier_list s = sepBy1 identifierp (match_str ",") <| s 

and selected_name_list s = sepBy1 selected_name (match_str ",") <| s

and subtype_indication s = tuple3 identifierp (opt identifierp) (opt constraintp) |>> SubtypeIndication <| s 

and constraintp s = (range_constraint |>> RangeConstraint) <|> (index_constraint |>> IndexConstraint) <| s

and range_constraint s = match_str "RANGE" >>. range <| s

and range s = (attribute_name |>> RangeAttributeName) <|> (tuple3 expr direction expr |>> RangeExpr) <| s

and direction s = choice [match_str "TO" >>% TO; match_str "DOWNTO" >>% DOWNTO] <| s

and index_constraint s = between (match_str "(") (match_str ")") discrete_range_list |>> IndexConstraintT <| s

and discrete_range_list s = sepBy1 discrete_range (match_str ",") <| s

and discrete_range s = ((subtype_indication |>> RangeWithSubtype) <|> (range |>> RangeWithConstraint)) <| s

and identifierp : Parser<Identifier, 'u> = fun s ->
    let isIdentifierFirstChar c = isLetter c
    let isIdentifierChar c = isLetter c || isDigit c || c = '_'
    (many1Satisfy2L isIdentifierFirstChar isIdentifierChar "identifier") >>=? (fun x -> 
        let str : String = x
        if keyWordSet.Contains(str.ToUpper()) then fail("unexpected keyword") else (preturn <| Identifier x .>> comments)) <| s

and selected_name s = 
    sepBy1 identifierp (match_str ".") .>>. optp (match_str "." >>. match_str "ALL") .>> comments >>= (fun res ->
        let (ids, opt_suffix) = res
        let suffix = if Option.isSome opt_suffix then "ALL" else ""
        preturn <| SelectedName (String.concat "." (List.map extractId ids) + suffix)) <| s

and attribute_name s = 
    tuple3 
        (prefix .>> match_str "'") 
        (match_str "RANGE" |>> Identifier  <|> identifierp) 
        (optp (between (match_str "(") (match_str ")") expr)) 
        |>> AttributeName <| s

and prefix s = 
    sepBy1 identifierp (match_str ".") >>= (fun ids -> 
        preturn <| Identifier (String.concat "." (List.map extractId ids))) <| s

and decimal_literal s = (many1 digit) .>> comments >>= (fun t -> preturn <| String.Concat(Array.ofList(t))) |>> Literal <| s

and private extractId (p: Identifier) =
    match p with
    | Identifier x -> x

and char_literal s = 
    between 
        (match_str "'") 
        (match_str "'") 
        ((manyMinMaxSatisfy2 2 2 ((=) '\\') (constant true)) <|> (manyMinMaxSatisfy 1 1 ((<>) '\''))) |>> CharLiteral <| s

and string_literal s =
    let str s = pstring s
    let normalCharSnippet = manySatisfy (fun c -> c <> '\\' && c <> '"')
    let escapedChar = str "\\" >>. (anyOf "\\\"nrt" |>> function
                                                        | 'n' -> "\n"
                                                        | 'r' -> "\r"
                                                        | 't' -> "\t"
                                                        | c   -> string c)
    between (str "\"") (str "\"")
            (stringsSepBy normalCharSnippet escapedChar) |>> StringLiteral <| s

and expr s = (decimal_literal |>> Expr.Literal) <|> (char_literal |>> Expr.CharLiteral) <|> (string_literal |>> Expr.StringLiteral) <| s
